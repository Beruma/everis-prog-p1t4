package dam1.prog.p1;
/*
 * Se pretende realizar realizar una librería que maneje las operaciones de un menú. Como mínimo ha de tener las
 * siguientes funciones.
 * - Mostrar opciones que se le pasaran en un array como parámetro.
 * - Pedir dato al usuario sobre opción de menú.
 * - Controlar la respuesta del usuario para que sea seguro que es un valor comprendido entre los valores que haya en
 * las opciones del menú.
 * - Cualquier otra funcionalidad que se considere necesaria.
 */

/**
 * @author Beatriz
 */

public class Librería {
    //muestras las opciones del menu//
    public static void mostrarOpciones(String[] menu) {
        for (String opciones : menu) {
            System.out.println(opciones);
        }
    }

    //te devuelve la opcion del menu//
    public static String pedirOpciones(String[] menu, int opcion) {
        return menu[opcion];
    }

    //te comprueba si existe esa opcion en el menu
    public static boolean comprobarOpcion(String[] menu, String opcion) {
        for (String opciones : menu) {

            if (opciones.equalsIgnoreCase(opcion)) {
                return true;
            }
        }
        return false;
    }

    //te muestra en que posicion esta esa opcion//
    public static boolean comprobarPosicionOpcion(String[] menu, int opcionPosicion, String opcion) {
        return menu[opcionPosicion].equals(opcion);
    }
}
